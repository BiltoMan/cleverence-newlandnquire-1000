package mono;
public class MonoPackageManager_Resources {
	public static String[] Assemblies = new String[]{
		/* We need to ensure that "Cleverence.Barcoding.Integration.dll" comes first in this list. */
		"Cleverence.Barcoding.Integration.dll",
		"Cleverence.Barcoding.dll",
		"Cleverence.Compact.Common.dll",
		"Cleverence.Integration.Plugin.dll",
		"NewlandJavaLibs.dll",
	};
	public static String[] Dependencies = new String[]{
	};
	public static String ApiPackageName = null;
}
