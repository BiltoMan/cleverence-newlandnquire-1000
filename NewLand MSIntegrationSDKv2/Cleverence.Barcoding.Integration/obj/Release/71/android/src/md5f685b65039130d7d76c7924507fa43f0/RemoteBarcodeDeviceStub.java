package md5f685b65039130d7d76c7924507fa43f0;


public class RemoteBarcodeDeviceStub
	extends md51fff0a7025e21f3810799bb61bb268e5.IRemoteBarcodeDeviceInterfaceStub
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Cleverence.Barcoding.Remote.RemoteBarcodeDeviceStub, Cleverence.Barcoding", RemoteBarcodeDeviceStub.class, __md_methods);
	}


	public RemoteBarcodeDeviceStub ()
	{
		super ();
		if (getClass () == RemoteBarcodeDeviceStub.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Remote.RemoteBarcodeDeviceStub, Cleverence.Barcoding", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
