#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Android.Runtime.ResourceDesignerAttribute("Cleverence.Barcoding.Integration.Resource", IsApplication=true)]

namespace Cleverence.Barcoding.Integration
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7F010000
			public const int Icon = 2130771968;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7F020000
			public const int buttonCreate = 2130837504;
			
			// aapt resource value: 0x7F020001
			public const int buttonDataMatrix = 2130837505;
			
			// aapt resource value: 0x7F020002
			public const int buttonDispose = 2130837506;
			
			// aapt resource value: 0x7F020003
			public const int buttonEan13 = 2130837507;
			
			// aapt resource value: 0x7F020004
			public const int buttonExit = 2130837508;
			
			// aapt resource value: 0x7F020005
			public const int buttonPdf417 = 2130837509;
			
			// aapt resource value: 0x7F020006
			public const int buttonSettings = 2130837510;
			
			// aapt resource value: 0x7F020007
			public const int buttonStartScan = 2130837511;
			
			// aapt resource value: 0x7F020008
			public const int buttonStopScan = 2130837512;
			
			// aapt resource value: 0x7F020009
			public const int linearLayout1 = 2130837513;
			
			// aapt resource value: 0x7F02000A
			public const int scrollView1 = 2130837514;
			
			// aapt resource value: 0x7F02000B
			public const int textDeviceId = 2130837515;
			
			// aapt resource value: 0x7F02000C
			public const int textHasKeyboard = 2130837516;
			
			// aapt resource value: 0x7F02000D
			public const int textResult = 2130837517;
			
			// aapt resource value: 0x7F02000E
			public const int textScannerStatus = 2130837518;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7F030000
			public const int LayoutTest = 2130903040;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7F040000
			public const int ApplicationName = 2130968576;
			
			// aapt resource value: 0x7F040001
			public const int Hello = 2130968577;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7F050000
			public const int Theme_Sherlock_Light = 2131034112;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
