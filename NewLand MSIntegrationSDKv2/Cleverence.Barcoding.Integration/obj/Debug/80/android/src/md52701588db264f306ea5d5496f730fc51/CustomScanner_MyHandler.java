package md52701588db264f306ea5d5496f730fc51;


public class CustomScanner_MyHandler
	extends android.os.Handler
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_handleMessage:(Landroid/os/Message;)V:GetHandleMessage_Landroid_os_Message_Handler\n" +
			"";
		mono.android.Runtime.register ("Cleverence.Barcoding.Integration.Plugin.CustomScanner+MyHandler, Cleverence.Integration.Plugin", CustomScanner_MyHandler.class, __md_methods);
	}


	public CustomScanner_MyHandler ()
	{
		super ();
		if (getClass () == CustomScanner_MyHandler.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner+MyHandler, Cleverence.Integration.Plugin", "", this, new java.lang.Object[] {  });
	}


	public CustomScanner_MyHandler (android.os.Handler.Callback p0)
	{
		super (p0);
		if (getClass () == CustomScanner_MyHandler.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner+MyHandler, Cleverence.Integration.Plugin", "Android.OS.Handler+ICallback, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public CustomScanner_MyHandler (android.os.Looper p0)
	{
		super (p0);
		if (getClass () == CustomScanner_MyHandler.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner+MyHandler, Cleverence.Integration.Plugin", "Android.OS.Looper, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public CustomScanner_MyHandler (android.os.Looper p0, android.os.Handler.Callback p1)
	{
		super (p0, p1);
		if (getClass () == CustomScanner_MyHandler.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner+MyHandler, Cleverence.Integration.Plugin", "Android.OS.Looper, Mono.Android:Android.OS.Handler+ICallback, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}

	public CustomScanner_MyHandler (md52701588db264f306ea5d5496f730fc51.CustomScanner p0)
	{
		super ();
		if (getClass () == CustomScanner_MyHandler.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner+MyHandler, Cleverence.Integration.Plugin", "Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", this, new java.lang.Object[] { p0 });
	}


	public void handleMessage (android.os.Message p0)
	{
		n_handleMessage (p0);
	}

	private native void n_handleMessage (android.os.Message p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
