package md52701588db264f306ea5d5496f730fc51;


public class CustomScanner
	extends md5f685b65039130d7d76c7924507fa43f0.RemoteBarcodeScannerBase
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", CustomScanner.class, __md_methods);
	}


	public CustomScanner ()
	{
		super ();
		if (getClass () == CustomScanner.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", "", this, new java.lang.Object[] {  });
	}

	public CustomScanner (android.content.Context p0)
	{
		super ();
		if (getClass () == CustomScanner.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
