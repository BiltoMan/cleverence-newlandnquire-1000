package md5f685b65039130d7d76c7924507fa43f0;


public abstract class RemoteBarcodeScannerBase
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Cleverence.Barcoding.Remote.RemoteBarcodeScannerBase, Cleverence.Barcoding", RemoteBarcodeScannerBase.class, __md_methods);
	}


	public RemoteBarcodeScannerBase ()
	{
		super ();
		if (getClass () == RemoteBarcodeScannerBase.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Remote.RemoteBarcodeScannerBase, Cleverence.Barcoding", "", this, new java.lang.Object[] {  });
	}

	public RemoteBarcodeScannerBase (android.content.Context p0)
	{
		super ();
		if (getClass () == RemoteBarcodeScannerBase.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Remote.RemoteBarcodeScannerBase, Cleverence.Barcoding", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
