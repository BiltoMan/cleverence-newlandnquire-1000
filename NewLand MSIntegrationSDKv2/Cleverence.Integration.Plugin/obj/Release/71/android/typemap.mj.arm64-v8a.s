	/* Data SHA1: 90862d9cb9ea6e3b247197c21bfe9f16b6b3c5df */
	.arch	armv8-a
	.file	"typemap.mj.inc"

	/* Mapping header */
	.section	.data.mj_typemap,"aw",@progbits
	.type	mj_typemap_header, @object
	.p2align	2
	.global	mj_typemap_header
mj_typemap_header:
	/* version */
	.word	1
	/* entry-count */
	.word	183
	/* entry-length */
	.word	181
	/* value-offset */
	.word	105
	.size	mj_typemap_header, 16

	/* Mapping data */
	.type	mj_typemap, @object
	.global	mj_typemap
mj_typemap:
	.size	mj_typemap, 33124
	.include	"typemap.mj.inc"
