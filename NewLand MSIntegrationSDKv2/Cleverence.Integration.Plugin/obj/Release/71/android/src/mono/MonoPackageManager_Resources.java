package mono;
public class MonoPackageManager_Resources {
	public static String[] Assemblies = new String[]{
		/* We need to ensure that "Cleverence.Integration.Plugin.dll" comes first in this list. */
		"Cleverence.Integration.Plugin.dll",
		"Cleverence.Barcoding.dll",
		"NewlandJavaLibs.dll",
		"Cleverence.Compact.Common.dll",
	};
	public static String[] Dependencies = new String[]{
	};
	public static String ApiPackageName = null;
}
