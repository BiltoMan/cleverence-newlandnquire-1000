package md52701588db264f306ea5d5496f730fc51;


public class CustomScanner
	extends md5f685b65039130d7d76c7924507fa43f0.RemoteBarcodeScannerBase
	implements
		mono.android.IGCUserPeer,
		com.example.bridgelib.ICleverenceHandler
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_OnScanedData:(Ljava/lang/String;)V:GetOnScanedData_Ljava_lang_String_Handler:Com.Example.Bridgelib.ICleverenceHandlerInvoker, NewlandJavaLibs\n" +
			"n_SomeChanged:(Ljava/lang/String;)V:GetSomeChanged_Ljava_lang_String_Handler:Com.Example.Bridgelib.ICleverenceHandlerInvoker, NewlandJavaLibs\n" +
			"";
		mono.android.Runtime.register ("Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", CustomScanner.class, __md_methods);
	}


	public CustomScanner ()
	{
		super ();
		if (getClass () == CustomScanner.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", "", this, new java.lang.Object[] {  });
	}

	public CustomScanner (android.content.Context p0)
	{
		super ();
		if (getClass () == CustomScanner.class)
			mono.android.TypeManager.Activate ("Cleverence.Barcoding.Integration.Plugin.CustomScanner, Cleverence.Integration.Plugin", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public void OnScanedData (java.lang.String p0)
	{
		n_OnScanedData (p0);
	}

	private native void n_OnScanedData (java.lang.String p0);


	public void SomeChanged (java.lang.String p0)
	{
		n_SomeChanged (p0);
	}

	private native void n_SomeChanged (java.lang.String p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
