using System;
using System.Collections.Generic;
using Android.Runtime;
using Java.Interop;

namespace Com.Example.Bridgelib {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.example.bridgelib']/class[@name='MyHandler']"
	[global::Android.Runtime.Register ("com/example/bridgelib/MyHandler", DoNotGenerateAcw=true)]
	public partial class MyHandler : global::Android.OS.Handler {

		internal    new     static  readonly    JniPeerMembers  _members    = new XAPeerMembers ("com/example/bridgelib/MyHandler", typeof (MyHandler));
		internal static new IntPtr class_ref {
			get {
				return _members.JniPeerType.PeerReference.Handle;
			}
		}

		public override global::Java.Interop.JniPeerMembers JniPeerMembers {
			get { return _members; }
		}

		protected override IntPtr ThresholdClass {
			get { return _members.JniPeerType.PeerReference.Handle; }
		}

		protected override global::System.Type ThresholdType {
			get { return _members.ManagedPeerType; }
		}

		protected MyHandler (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.example.bridgelib']/class[@name='MyHandler']/constructor[@name='MyHandler' and count(parameter)=1 and parameter[1][@type='com.example.bridgelib.ICleverenceHandler']]"
		[Register (".ctor", "(Lcom/example/bridgelib/ICleverenceHandler;)V", "")]
		public unsafe MyHandler (global::Com.Example.Bridgelib.ICleverenceHandler cleverenceHandler)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			const string __id = "(Lcom/example/bridgelib/ICleverenceHandler;)V";

			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JniArgumentValue* __args = stackalloc JniArgumentValue [1];
				__args [0] = new JniArgumentValue ((cleverenceHandler == null) ? IntPtr.Zero : ((global::Java.Lang.Object) cleverenceHandler).Handle);
				var __r = _members.InstanceMethods.StartCreateInstance (__id, ((object) this).GetType (), __args);
				SetHandle (__r.Handle, JniHandleOwnership.TransferLocalRef);
				_members.InstanceMethods.FinishCreateInstance (__id, this, __args);
			} finally {
			}
		}

	}
}
