using System;
using System.Collections.Generic;
using Android.Runtime;
using Java.Interop;

namespace Com.Example.Bridgelib {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.example.bridgelib']/interface[@name='ICleverenceHandler']"
	[Register ("com/example/bridgelib/ICleverenceHandler", "", "Com.Example.Bridgelib.ICleverenceHandlerInvoker")]
	public partial interface ICleverenceHandler : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.example.bridgelib']/interface[@name='ICleverenceHandler']/method[@name='OnScanedData' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("OnScanedData", "(Ljava/lang/String;)V", "GetOnScanedData_Ljava_lang_String_Handler:Com.Example.Bridgelib.ICleverenceHandlerInvoker, NewlandJavaLibs")]
		void OnScanedData (string p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.example.bridgelib']/interface[@name='ICleverenceHandler']/method[@name='SomeChanged' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("SomeChanged", "(Ljava/lang/String;)V", "GetSomeChanged_Ljava_lang_String_Handler:Com.Example.Bridgelib.ICleverenceHandlerInvoker, NewlandJavaLibs")]
		void SomeChanged (string p0);

	}

	[global::Android.Runtime.Register ("com/example/bridgelib/ICleverenceHandler", DoNotGenerateAcw=true)]
	internal class ICleverenceHandlerInvoker : global::Java.Lang.Object, ICleverenceHandler {

		internal    new     static  readonly    JniPeerMembers  _members    = new JniPeerMembers ("com/example/bridgelib/ICleverenceHandler", typeof (ICleverenceHandlerInvoker));

		static IntPtr java_class_ref {
			get { return _members.JniPeerType.PeerReference.Handle; }
		}

		public override global::Java.Interop.JniPeerMembers JniPeerMembers {
			get { return _members; }
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return _members.ManagedPeerType; }
		}

		IntPtr class_ref;

		public static ICleverenceHandler GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ICleverenceHandler> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.example.bridgelib.ICleverenceHandler"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ICleverenceHandlerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_OnScanedData_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetOnScanedData_Ljava_lang_String_Handler ()
		{
			if (cb_OnScanedData_Ljava_lang_String_ == null)
				cb_OnScanedData_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnScanedData_Ljava_lang_String_);
			return cb_OnScanedData_Ljava_lang_String_;
		}

		static void n_OnScanedData_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Example.Bridgelib.ICleverenceHandler __this = global::Java.Lang.Object.GetObject<global::Com.Example.Bridgelib.ICleverenceHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnScanedData (p0);
		}
#pragma warning restore 0169

		IntPtr id_OnScanedData_Ljava_lang_String_;
		public unsafe void OnScanedData (string p0)
		{
			if (id_OnScanedData_Ljava_lang_String_ == IntPtr.Zero)
				id_OnScanedData_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "OnScanedData", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_OnScanedData_Ljava_lang_String_, __args);
			JNIEnv.DeleteLocalRef (native_p0);
		}

		static Delegate cb_SomeChanged_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSomeChanged_Ljava_lang_String_Handler ()
		{
			if (cb_SomeChanged_Ljava_lang_String_ == null)
				cb_SomeChanged_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SomeChanged_Ljava_lang_String_);
			return cb_SomeChanged_Ljava_lang_String_;
		}

		static void n_SomeChanged_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Example.Bridgelib.ICleverenceHandler __this = global::Java.Lang.Object.GetObject<global::Com.Example.Bridgelib.ICleverenceHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SomeChanged (p0);
		}
#pragma warning restore 0169

		IntPtr id_SomeChanged_Ljava_lang_String_;
		public unsafe void SomeChanged (string p0)
		{
			if (id_SomeChanged_Ljava_lang_String_ == IntPtr.Zero)
				id_SomeChanged_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "SomeChanged", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_SomeChanged_Ljava_lang_String_, __args);
			JNIEnv.DeleteLocalRef (native_p0);
		}

	}

}
